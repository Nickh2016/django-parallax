# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
from djangocmsparallax import __version__


setup(
    name='djangocmsparallax',
    version=__version__,
    description=open('README.rst').read(),
    author='Nick Holland',
    author_email='kungfunick.nh@googlemail.com',
    packages=find_packages(),
    platforms=['OS Independent'],
    install_requires=[],
    include_package_data=True,
    zip_safe=False,
)
